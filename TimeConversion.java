package Hackerrank;

public class TimeConversion {
    static String timeConversion(String s) {
        String convertedTimeString="";
        String hourString = s.substring(0, 2);
        String time = s.substring(2, s.length()-2);
        int hour = Integer.valueOf(hourString);
        if(s.contains("AM")){
            if(hour==12){
                hour=00;
                convertedTimeString = "00".concat(time);
            }else if(hour<10){
                convertedTimeString = "0".concat(String.valueOf(hour).concat(time));
            }
        }else if(s.contains("PM")){
            if(hour!=12){
                hour+=12;
                convertedTimeString = String.valueOf(hour).concat(time);
            }else{
                convertedTimeString = String.valueOf(hour).concat(time);
            }
        }
        return convertedTimeString;
    }
}
