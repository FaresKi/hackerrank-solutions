package Hackerrank;

public class JumpingOnClouds {
    static int jumpingOnClouds(int[] c) {
        int numberOfSteps = 0;
        int currentCloud = 0;
        while (currentCloud < c.length - 1) {
            System.out.println("Current cloud " + currentCloud);
            if (c[currentCloud + 1] == 1) {
                System.out.println("Thunderhead right in front of me");
                currentCloud += 2;
                numberOfSteps++;
            } else if (currentCloud+2 < c.length - 1 && c[currentCloud+2]==1) {
                System.out.println("Thunderhead is after the next cloud");
                currentCloud++;
                numberOfSteps++;
            } else {
                System.out.println("No thunderheads ahead");
                currentCloud += Math.min(2, (c.length - 1) - currentCloud);
                numberOfSteps++;
            }
        }
        return numberOfSteps;
    }
}
