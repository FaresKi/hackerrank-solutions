package Hackerrank;

public class RepeatedString {
    static long repeatedString(String s, long n) {
        long counter = 0;
        long remainder = 0;
        long total = 0;
        long numberOfOccurences = (n / s.length());
        int sRemainingLetters = (int) n % s.length();
        for (char c : s.toCharArray()) {
            if (c == 'a') {
                counter++;
            }
        }
        for (char c : s.substring(0, sRemainingLetters).toCharArray()) {
            if (c == 'a') {
                remainder++;
            }
        }
        total = numberOfOccurences * counter + remainder;
        return total;
    }

    public static void main(String[] args) {
        System.out.println(10 % 3);
    }
}
