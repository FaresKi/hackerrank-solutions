package Hackerrank;

import java.util.ArrayList;
import java.util.Collections;

public class MinMaxSum {
    static void miniMaxSum(int[] arr) {
        ArrayList<Integer> numbers = new ArrayList<>(arr.length);
        for(int i = 0; i < arr.length; i++){
            numbers.add(arr[i]);
        }
        Collections.sort(numbers);
        long minSum = 0, maxSum = 0;

        for(int i = 0; i<numbers.size()-1;i++){
            minSum += numbers.get(i);
        }

        for(int i = 1; i<numbers.size(); i++){
            maxSum+=numbers.get(i);
        }

        System.out.println(minSum + " " + maxSum);
        
    }
}
