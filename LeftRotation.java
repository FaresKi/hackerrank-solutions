package Hackerrank;

public class LeftRotation {
    static int[] rotLeft(int[] a, int d) {
        int[] rotatedArray = new int[a.length];
        int index = 0;
        int rotatedIndex = d;

        //first elements to fetch are the shifted elements.
        while(rotatedIndex<a.length){
            rotatedArray[index] = a[rotatedIndex];
            index++ ;
            rotatedIndex++;
        }

        //once those elements fetched, fetch first initial elements. 
        rotatedIndex=0;
        while(rotatedIndex<d){
            rotatedArray[index] = a[rotatedIndex];
            index++ ;
            rotatedIndex++;
        }
        return rotatedArray;
    }
}
