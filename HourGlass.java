package Hackerrank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class HourGlass {
    static int hourglassSum(int[][] arr) {
        //16 hour glasses in a 6x6 array
        HashMap<ArrayList<Integer>, Integer> hourGlasses = new HashMap<>();
        int i =0, j=0;
        boolean isDone = false;
        while(!isDone){
            //search for hourglasses
            if(j-1>=0 && j+1<=5){
                if(i+2<=5){
                    //add possible hourglasses
                    System.out.println("Show me i : " + i);
                    System.out.println("Show me j : " + j);
                    ArrayList<Integer> hourGlass = new ArrayList<>();
                    hourGlass.add(arr[i][j-1]);
                    hourGlass.add(arr[i][j]);
                    hourGlass.add(arr[i][j+1]);
                    hourGlass.add(arr[i+1][j]);
                    hourGlass.add(arr[i+2][j-1]);
                    hourGlass.add(arr[i+2][j]);
                    hourGlass.add(arr[i+2][j+1]);
                    //add all elements in hourglasses
                    int sum = hourGlass.stream().mapToInt(Integer::intValue).sum();
                    if(hourGlasses.get(hourGlass)==null){
                        hourGlasses.put(hourGlass, sum);
                    }
                    System.out.println("added hourglass and its key : " + hourGlass );
                    j++;
                    sum =0;
                    hourGlass = new ArrayList<>();
                }else if(i==5 && j==5){
                    isDone=true;
                }
            }else if(j==5){
                i++;
                j=0;
            }else{
                j++;
            }
        }
        return Collections.max(hourGlasses.values());
    }
    
}
