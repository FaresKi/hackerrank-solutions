package Hackerrank;

import java.util.ArrayList;
import java.util.List;

public class GradingStudents {

    /**
     * if grades < 40 --> don't change difference between grade and next multiple of
     * 5 < 5 --> round up.
     * 
     * @param grades
     * @return
     */
    public static List<Integer> gradingStudents(List<Integer> grades) {
        // Write your code here
        List<Integer> newGrades = new ArrayList<>(grades.size());
        for (int grade : grades) {
            int i = 0;
            if (grade >= 38) {
                while (!((grade + i) % 5 == 0)) {
                    i++;
                }
                if (i < 3) {
                    newGrades.add(grade + i);
                } else {
                    newGrades.add(grade);
                }
            } else {
                newGrades.add(grade);
            }
        }
        return newGrades;

    }

}
