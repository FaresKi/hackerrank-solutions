package Hackerrank;

public class MinimalBribes {

    static void minimumBribes(int[] q) {
        int ans = 0;
        for (int i = q.length - 1; i >= 0; i--) {
            int changedPosition = q[i] - (i + 1);
            //we don't care, the shift is too big
            if (changedPosition > 2) {
                System.out.println("Too chaotic");
                return;
            // 
            } else {
                int st = Math.max(0, q[i] - 2);
                for (int j = st; j < i; j++) {
                    if (q[j] > q[i])
                        ans++;
                }
            }
        }
        System.out.println(ans);
    }
    public static void main(String[] args) {

    }
}