package Hackerrank;

import java.util.Collections;
import java.util.List;

public class BirthdayCakeCandles {
    public static int birthdayCakeCandles(List<Integer> candles) {
        int numberOfHighestCandles = candles.size();
        Collections.sort(candles, Collections.reverseOrder());
        if(candles.get(0)==candles.get(candles.size()-1)){
            return numberOfHighestCandles;
        }else{
            numberOfHighestCandles=0;
            for(int candle : candles){
                if(candle==candles.get(0)){
                    numberOfHighestCandles++;
                }else{
                    break;
                }
            }
        }
        return numberOfHighestCandles;
    }

}
